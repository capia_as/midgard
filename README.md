### What is this repository for? ###

* Generate pleasant random data

### How do I get set up? ###

* library(devtools)
* install_bitbucket()
* midgart::generate()
