# Midpoint Displacement Artificial Generated Data
#
#
# Some useful keyboard shortcuts for package authoring:
#
#   Build and Reload Package:  'Ctrl + Shift + B'
#   Check Package:             'Ctrl + Shift + E'
#   Test Package:              'Ctrl + Shift + T'


#' Generate random data
#'
#' @author Stian Berger
#'
#' @param width Width of dataset
#' @param start Starting point
#' @param stop Endpoint
#' @param displace Amount of movement of data
#' @param roughness Roughness of data / How rapid displacement falls off
#' @param seed A random seed, to make it possible to get same base data every time.
#'
#' @return
#' @export
#'
#' @examples
generate <- function(width = 512, amplitude = 10, roughness = 0.5, seed = NULL) {
  if(!is.null(seed)) { set.seed(seed) }
  p <- vector()
  w <- 2^(ceiling(log(width) / log(2)))
  p[1] <- 0
  p[w] <- 0
  i <- 1
  while(i < w) {
    j <- (w / i)/2
    while(j < w) {
      nextCell <- j + (w / i) / 2
      prevCell <- max(j - (w / i) / 2, 1)
      p[j] <- (p[prevCell] + p[nextCell]) / 2
      p[j] <- p[j] + (runif(1) * amplitude * 2) - amplitude
      j <- j + (w / i)
    }
    amplitude <- amplitude * roughness;
    i <- i * 2
  }
  return(p)
}


#' Add seasonal trend to data
#' #' Can chain multiple seasonalities together
#' 
#' @param data 
#' @param frequency Frequency is given in parts of a year. year = 1, week = 52, daily = 365 and so forth.
#' @param amplitude How much of an effect
#' @param phase For yearly frequency, phase = 0, year starts at 0 and increases until peak in spring 0 in summer and bottom at autumn. 
#'   A phase of 90 the peak will be summer and bottom winter, and opposite for -90.
#' @return
#' @export
#'
#' @examples
season <- function(data, frequency = 1, amplitude = 10, phase = 0) {
  return (data + (sin((1:length(data) * (6.28/(365.25/frequency))) + (phase * (6.28/(365.25/frequency)))) * amplitude))
}

#' Add an offset to the data. Value will be added to all data.
#' x + a
#' @param data 
#' @param offset 
#'
#' @return
#' @export
#'
#' @examples
offset <- function(data, offset) {
  return (data + offset)
}


#' Insert a linear trend to the data
#' x + (t * a)
#' Where this function controls "a" and b is set by offset function.
#' 
#' @param data 
#' @param factor 
#'
#' @return
#' @export
#'
#' @examples
linear_trend <- function(data, factor = 0) {
  #return (data + seq(from, to, length.out = length(data)))
  return (data + (1:length(data)) * factor)
}

#' Add power trend to data
#' x + t ^ a
#' @param data 
#' @param factor 
#'
#' @return
#' @export
#'
#' @examples
power_trend <- function(data, factor = 1) {
  return (data + (1:length(data)) ^ factor)
}

#' Add log trend to data
#' x + log(t) * a
#' @param data 
#' @param factor 
#'
#' @return
#' @export
#'
#' @examples
log_trend <- function(data, factor = 1) {
  return (data + log(1:length(data)) * factor)
}

#' Add exponential trend to data
#' x + a ^ t
#' @param data 
#' @param factor 
#'
#' @return
#' @export
#'
#' @examples
exponential_trend <- function(data, factor = 1) {
  return (data + factor ^ (1:length(data)))
}

weekend <- function(data, factor = 1) {
  
}

vacation <- function(data, start, end, factor) {
  
}