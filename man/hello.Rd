\name{midgard}
\alias{midgard}
\title{MIdpoint Displaced Generated Artificial Random Data}
\usage{
generate()
}
\description{
Returns a pleasant random data series.
}
\examples{
generate()
}
